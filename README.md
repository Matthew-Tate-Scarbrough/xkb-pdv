<!--
  vim: tabstop=4 softtabstop=4 shiftwidth=4 expandtab textwidth=72
  vim: colorcolumn=72
  -->

# X11/xkb/symbols/pdv

A family of keyboard layouts for Xorg's keyboard extension (XKB) for the
Programmer's Dvorak keyboard layout.  Like the proper Kaufmann
layout[^1], it doesn't use the 105th key found on ISO keyboards.

## Layouts

The basic layout is no different from that of the `pdv` variant of the
`us` layout.

It is invoked via

``` sh
setxkbmap -layout pdv
```

There are two recommended variants, though.  These are `kaufmann`
(`dvp`) and `scarbrough` (`ling-pdv`).

**Kaufmann Layout**

    ┌─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┲━━━━━━━━━┓
    │ ~   │ %   │ 7   │ 5   │ 3   │ 1   │ 9   │ 0   │ 2   │ 4   │ 6   │ 8   │ `   ┃Backspace┃
    │ $   │ &   │ [   │ { € │ }   │ ( ¢ │ =   │ *   │ )   │ +   │ ]   │ !   │ #   ┃ ⌫       ┃
    ┢━━━━━┷━┱───┴─┬───┴─┬───┴─┬───┴─┬───┴─┬───┴─┬───┴─┬───┴─┬───┴─┬───┴─┬───┴─┬───┺━┯━━━━━━━┩
    ┃Tab    ┃ :   │ <   │ >   │ P   │ Y Ü │ F   │ G   │ C Ç │ R   │ L   │ ?   │ ^   │ |     │
    ┃ ↹     ┃ ; ◌̈ │ , « │ . » │     │     │     │     │     │     │     │ /   │ @   │ \     │
    ┣━━━━━━━┻┱────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┲━━━━┷━━━━━━━┪
    ┃Caps    ┃ A Å │ O Ø │ E Æ │ U É │ I   │ D   │ H ◌́ │ T   │ N Ñ │ S   │ _   ┃ ⏎          ┃
    ┃Lock  ⇬ ┃     │     │     │     │     │     │     │     │     │   ß │ -   ┃ Enter      ┃
    ┣━━━━━━━━┻━━━┱─┴───┬─┴───┬─┴───┬─┴───┬─┴───┬─┴───┬─┴───┬─┴───┬─┴───┬─┴───┲━┻━━━━━━━━━━━━┫
    ┃Shift       ┃ "   │ Q   │ J   │ K   │ X   │ B   │ M   │ W   │ V   │ Z   ┃Shift         ┃
    ┃ ⇧          ┃ '   │     │     │     │     │     │     │     │     │     ┃ ⇧            ┃
    ┣━━━━━━━┳━━━━┻━━┳━━┷━━━━┳┷━━━━━┷━━━━━┷━━━━━┷━━━━━┷━━━━━┷━━━━┳┷━━━━━╈━━━━━┻┳━━━━━━┳━━━━━━┫
    ┃Ctrl   ┃ Meta  ┃Alt    ┃                                   ┃AltGr ┃ R    ┃ Ctxt ┃ Ctrl ┃
    ┃       ┃       ┃       ┃                                   ┃      ┃ Meta ┃ Menu ┃      ┃
    ┗━━━━━━━┻━━━━━━━┻━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┻━━━━━━┻━━━━━━┻━━━━━━┻━━━━━━┛

**Scarbrough Layout**

    ┌─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┬─────┲━━━━━━━━━┓
    │ ~ õ │ %   │ 7   │ 5 ‹ │ 3 › │ 1 ≤ │ 9   │ 0 å │ 2 ≥ │ 4   │ 6   │ 8 ‽ │ ` ò ┃Backspace┃
    │ $ £ │ &   │ [   │ { « │ } » │ ( ⟨ │ = ≠ │ * × │ ) ⟩ │ + ‡ │ ]   │ ! ¡ │ # ;P┃ ⌫       ┃
    ┢━━━━━┷━┱───┴─┬───┴─┬───┴─┬───┴─┬───┴─┬───┴─┬───┴─┬───┴─┬───┴─┬───┴─┬───┴─┬───┺━┯━━━━━━━┩
    ┃Tab    ┃ : ö │ < ‘ │ > ’ │ P   │ Y   │ F   │ G   │ C © │ R ® │ L ǫ │ ? ¿ │ ^ ô │ |     │
    ┃ ↹     ┃ ; ȯ │ , “ │ . ” │   þ │     │     │   ȝ │   ¢ │   ¶ │   → │ / ÷ │ @ ⌀ │ \ ø   │
    ┣━━━━━━━┻┱────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┬────┴┲━━━━┷━━━━━━━┪
    ┃Caps    ┃ A   │ O   │ E € │ U   │ I   │ D   │ H ⇄ │ T ™ │ N   │ S § │ _ — ┃ ⏎          ┃
    ┃Lock  ⇬ ┃   æ │   œ │   ε │     │   ı │   ð │   ← │   † │   ŋ │   ſ │ - – ┃ Enter      ┃
    ┣━━━━━━━━┻━━━┱─┴───┬─┴───┬─┴───┬─┴───┬─┴───┬─┴───┬─┴───┬─┴───┬─┴───┬─┴───┲━┻━━━━━━━━━━━━┫
    ┃Shift       ┃ " „ │ Q   │ J ⇅ │ K ç │ X   │ B ß │ M ō │ W   │ V   │ Z   ┃Shift         ┃
    ┃ ⇧          ┃ ' ó │     │   ↓ │   ↑ │     │   ǒ │   µ │   ƿ │     │   ʒ ┃ ⇧            ┃
    ┣━━━━━━━┳━━━━┻━━┳━━┷━━━━┳┷━━━━━┷━━━━━┷━━━━━┷━━━━━┷━━━━━┷━━━━┳┷━━━━━╈━━━━━┻┳━━━━━━┳━━━━━━┫
    ┃Ctrl   ┃ Meta  ┃Alt    ┃                                   ┃AltGr ┃ R    ┃ Ctxt ┃ Ctrl ┃
    ┃       ┃       ┃       ┃                                   ┃      ┃ Meta ┃ Menu ┃      ┃
    ┗━━━━━━━┻━━━━━━━┻━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┻━━━━━━┻━━━━━━┻━━━━━━┻━━━━━━┛

If you have used the Kaufmann layout for some time, or intend on using
the official Programmer's Dvorak on Windows and/or Mac, then you will
want to use the `kaufmann` layout.  However, if you are new to the
Programmer's Dvorak or you intend upon using the multi-lingual features,
you will probably want to use my `scarbrough` layout.

To use, you can add the following to your `~/.xinitrc`.

``` sh
# Kaufmann
setxkbmap -layout pdv -variant kaufmann   # -variant dvp

# Scarbrough
setxkbmap -layout pdv -variant scarbrough # -variant ling-pdv
```

## How to install

**Cloning**

``` sh
# Make file if not exists
mkdir -p $HOME/.local/src

# Clone repo
git clone https://gitlab.com/Matthew-Tate-Scarbrough/xkbd-pdv $HOME/.local/src/xkb-pdv
```

**Installing**

In order to install, either clone or sym-link the `pdv` file to
`X11/xkb/symbols/`.  On both my Void Linux and OpenSUSE Tumbleweed
systems, this is located in `/usr/share/X11/xkb/symbols/`.  Your's may
be different.

If you would like to edit the file without needing root permissions,
then do:

``` sh
# Create a link from DESTINATION to LINK
sudo ln -s \
    /home/"$USERNAME"/.local/src/xkb-pdv/pdv \ # DEST
    /usr/share/X11/xkb/symbols/              \ # LINK -- the final `/`
                                               #         is important
```

Just note that by so doing, you have to be super careful if you are in
the middle of doing heavy modifications of the file.  You may be without
your keyboard layout until all the issues are sorted out.

If you want a safer, though more repetitive method, then you should just
copy the file into the directory:

``` sh
# Copy file.  If it exists, overwrite it.
sudo cp -f \
    /home/"$USERNAME"/.local/src/xkb-pdv/pdv \ # DEST
    /usr/share/X11/xkb/symbols/              \ # LINK -- the final `/`
                                               #         is important
```

This method is safer.

Now that it is installed, you will either need to call it in your
`~/.xinitrc` or need to modify `X11/xkb/rules/evdev.xml` to be able to
access the keymap when using a desktop environment like GNOME40 or KDE
Plasma 5.

**Editing X11/xkb/rules/evdev.xml**

You will need to be root to do this.  If you only intend on editing
pre-existing files, then you need not modify the text in
`evdev-additions.xml`.  If you want to add your own layouts, then you
should modify the additions.  All you will be modifying is copying and
pasting a `<variant></variant>` nest and changing the contents to match
your additional layout.  It is fairly straightforward.

To modify `evdev.xml`, open it as root in your favourite texteditor:

``` sh
sudo gedit /usr/share/X11/xkb/rules/evdev.xml
```

You will also open the `evdev-additions.xml` file (preferrably as a
normal user.)  If you are making modifications to the file (to suit your
own custom layouts you've added), then you should add them to the
`evdev-additions.xml` file and save them as a normal user.  This will
give you a current backup in case in the future a system update
overwrites `X11/.../evdev.xml`.

Copy and paste them into place after `<layoutList>` and before the
first `<layout>` tag.  You can technically put it anywhere within the
`<layoutList></layoutList>` tags so long as it is outside of all
`<layout></layout>` tags, but we want it to be easy to find, no?

``` xml
       <vendor>Google</vendor>
     </configItem>
    </model>
  </modelList>
  <layoutList>
    <!-- HERE IS WHERE IT WILL GO -->
    <layout>
      <configItem>
        <name>us</name>
        <!-- Keyboard indicator for English layouts -->
        <shortDescription>en</shortDescription>
        <description>English (US)</description>
        <languageList>
          <iso639Id>eng</iso639Id>
        </languageList>
      </configItem>
      <variantList>
        <variant>
```

For me, this is between lines 1334 and 1335.  Here is an after-shot:

``` xml
       <vendor>Google</vendor>
     </configItem>
    </model>
  </modelList>
  <layoutList>
    <layout> <!-- START -->
      <configItem>
        <name>pdv</name>
        <!-- Keyboard indicator for Programmer's Dvorak layouts -->
        <shortDescription>pdv</shortDescription>
        <description>English (Programmer's Dvorak)</description>
        <languageList>
          <iso639Id>eng</iso639Id>
        </languageList>
      </configItem>
      <variantList>
        <variant>
          <configItem>
            <name>kaufmann</name>
            <!-- Keyboard indicator for the Kaufmann layout -->
            <shortDescription>dvp</shortDescription>
            <description>Programmer's Dvorak (Kaufmann)</description>
          </configItem>
        </variant>
        <variant>
          <configItem>
            <name>scarbrough</name>
            <!-- Keyboard indicator for the Scarbrough layout -->
            <shortDescription>lpdv</shortDescription>
            <description>Programmer's Dvorak (Scarbrough)</description>
          </configItem>
        </variant>
      </variantList>
    </layout> <!-- END -->
    <layout>
      <configItem>
        <name>us</name>
        <!-- Keyboard indicator for English layouts -->
        <shortDescription>en</shortDescription>
        <description>English (US)</description>
        <languageList>
          <iso639Id>eng</iso639Id>
        </languageList>
      </configItem>
      <variantList>
        <variant>
```

After this, you will need to log out or restart your computer.

## Languages

  + [x] English (default)
  + [x] Greek
  + [ ] Russian (finished but not committed)
  + [ ] Southern Altai (part of the Russian)
  + [ ] International Phonetic Alphabet

## Footnotes

[^1]: <https://www.kaufmann.no/roland/dvorak/>
